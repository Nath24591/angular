var app = angular.module('myApp', ['ui.bootstrap']);

app.controller('ListController', function($scope, $http){

    $scope.curPage = 1;
    $scope.itemsPerPage = 10;

    $scope.cams = []; //declare an empty array
    $http.get("http://api.oceandrivers.com/v1.0/getWebCams/").success(function(response){
        $scope.cams = response;  //ajax request to fetch data into $scope.cams
        $scope.numOfPages = function () {
            return Math.ceil($scope.cams.length / $scope.itemsPerPage);
        };
        $scope.$watch('curPage + numPerPage', function() {
            console.log($scope.cams);
            var begin = (($scope.curPage - 1) * $scope.itemsPerPage),
                end = begin + $scope.itemsPerPage;

            $scope.filteredItems = $scope.cams.slice(begin, end);
        });
    });
});

